/**
 * SpecialChequingAccount.java
 * by Philippe Nadon
 * 22 February 2018 (last modified 23 Feb 2018)
 *
 * Public Methods:
 *    Constructors to make a ChequingAccount
 *    withdrawal(double) --> boolean
 *        Subtracts from balance if amount positive (takes out withdrawal
 *        fee). Sets applyServiceFee to true upon successful withdrawal, 
 *        so there is a fee applied the next time.
 *    endOfMonth()
 *        Performs month end stuff (interest calc, reset variables,
 *        print report, and disable service fees).
 *        
 */

package bankingassignment;

public class SavingsAccount extends GeneralAccount {
  
  //===========================================
  //METHODS

  /**************************************
   * Constructors
   *    Creates account, sets interestRate to 0.002 (0.2%) and
   *    withdrawFee to 5.00.
   *
   * @param accountNum = the account number, given as a String.
   * @param fName = the first name of account holder, given as a String.
   * @param lName = the last name of account holder, given as a String.
   * @param initBalance = the initial deposit amount (must be +ve or set to 0)
   */
  public SavingsAccount(String accountNum, String fName, String lName, 
      double initBalance){
    super(accountNum, fName, lName, initBalance);
    interestRate = 0.002;
    withdrawFee = 5.00;
  }//Constructor, all arguments
  
  public SavingsAccount(){
    this("0","Unknown","Unknown",0);
  }//default constructor, no arguments given
  
  @Override
  /**************************************
   * withdrawal
   *    Subtract amount (and service fee) from balance, assuming amount
   *    is positive and service fee (withdrawFee) is available in account.
   *    Sets applyServiceFee to true upon successful withdrawal, so there
   *    is a fee applied the next time.
   *
   * @param amount = positive double number
   * @return: true if transaction successful; false otherwise
   */
  public boolean withdrawal(double amount){
    if( super.withdrawal(amount) ){
      applyServiceFee = true;
      return true;
    }
    return false;
  }//withdrawal
  
  @Override
  /**************************************
   * endOfMonth
   *    Adds interest to balance, prints report, and disables
   *    service fees.
   */
  public void endOfMonth(){
    super.endOfMonth();
    applyServiceFee = false;
  }//endOfMonth
  
}
