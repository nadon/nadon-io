/**
 * SpecialChequingAccount.java
 * by Philippe Nadon
 * 22 February 2018 (last modified 23 Feb 2018)
 *
 * Public Methods:
 *    Constructors to make a SpecialChequingAccount
 *    withdrawal(double) --> boolean
 *        Subtracts from balance if amount positive (takes out withdrawal
 *        fee if minimum month balance is les than 1000)
 *    endOfMonth()
 *        Performs month end stuff (interest calc, reset variables,
 *        print report) and disables service fees if minimum month balance is
 *        is greater than 1000.
 *    billPayment(double amount, GeneralAccount g) --> boolean
 *        Subtracts from balance the amount and the billFee (if the
 *        minimum month balance is less than 1000) and the amount into the
 *        account specified.
 *    chequeCashed(int num, double amount) --> boolean
 *        Takes the amount out of the account, applies a fee if the
 *        minimum monthly balance is less than 1000.
 */

package bankingassignment;

public class SpecialChequingAccount extends ChequingAccount {
  
  //===========================================
  //METHODS

  /**************************************
   * Constructors
   *    Creates account, enables fees if minMonthBalance is less than 1000.
   *
   * @param accountNum = the account number, given as a String.
   * @param fName = the first name of account holder, given as a String.
   * @param lName = the last name of account holder, given as a String.
   * @param initBalance = the initial deposit amount (must be +ve or set to 0)
   */
  public SpecialChequingAccount(String accountNum, String fName, String lName, 
      double initBalance){
    super(accountNum, fName, lName, initBalance);
    applyServiceFee = minMonthBalance < 1000; 
  }//Constructor, all arguments
  
  public SpecialChequingAccount(){
    this("0","Unknown","Unknown",0);
  }//default constructor, no arguments given
  
  @Override
  /**************************************
   * withdrawal
   *    Subtract amount (and service fee) from balance, assuming amount
   *    is positive and service fee (withdrawFee) is available in account.
   *    Also enables service fees if minMonthBalance is less than 1000.
   *
   * @param amount = positive double number
   * @return: true if transaction successful; false otherwise
   */
  public boolean withdrawal(double amount){
    boolean res = super.withdrawal(amount);
    if(minMonthBalance < 1000){
      applyServiceFee = true;
    }
    return res;
  }//withdrawal
  
  @Override
  /**************************************
   * endOfMonth
   *    Prints report, wipes record of cheques cashed, and resets service fees
   *    if the minMonthBalance is greater than 1000.
   */
  public void endOfMonth(){
    super.endOfMonth();
    applyServiceFee = minMonthBalance < 1000;
  }//endOfMonth
  
  @Override
  /**************************************
   * billPayment
   *    Subtracts from balance the amount and the billFee (if the
   *    minimum month balance is less than 1000) and places the amount into 
   *    the account specified.
   *
   * @param amount = amount of the bill (positive double)
   * @param g = account to pay
   * @return true if the bill payment was successful
   */
  public boolean billPayment(double amount, GeneralAccount g){
    boolean res = super.billPayment(amount, g);
    if(minMonthBalance < 1000){
      applyServiceFee = true;
    }
    return res;
  }//billPayment

  @Override
  /**************************************
   * chequeCashed
   *    Takes the amount out of the account, applies a fee if the
   *    minimum monthly balance is less than 1000.
   *
   * @param num = the cheque number (an int)
   * @param amount = amount of the cheque (positive double)
   * @return true if the cheque was successfully cashed
   */
  public boolean chequeCashed(int num, double amount){
    boolean res = super.chequeCashed(num, amount);
    if(minMonthBalance < 1000){
      applyServiceFee = true;
    }
    return res;
  }//chequeCashed
  
}
