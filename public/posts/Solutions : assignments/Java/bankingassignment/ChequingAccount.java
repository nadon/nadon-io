/**
 * SpecialChequingAccount.java
 * by Philippe Nadon
 * 22 February 2018 (last modified 23 Feb 2018)
 *
 * Public Methods:
 *    Constructors to make a ChequingAccount
 *    billPayment(double amount, GeneralAccount g) --> boolean
 *        Subtracts from balance the amount and the billFee (if the
 *        minimum month balance is less than 1000) and the amount into the
 *        account specified.
 *    chequeCashed(int num, double amount) --> boolean
 *        Takes the amount out of the account, applies a fee if the
 *        minimum monthly balance is less than 1000.
 *    endOfMonth()
 *        Performs month end stuff (interest calc, reset variables,
 *        print report) and disables service fees if minimum month balance is
 *        is greater than 1000.
 *    toString()
 *        Gives String representation of an account
 *        
 */

package bankingassignment;

public class ChequingAccount extends GeneralAccount {

  //===========================================
  //DATA
  
  double billFee;
  double chequeFee;
  String chequesCashed = "";

  //===========================================
  //METHODS

  /**************************************
   * Constructors
   *    Creates account, sets applyServiceFee to true, billFee to 5.50,
   *    chequeFee to 3.50, and withdrawFee to 3.50.
   *
   * @param accountNum = the account number, given as a String.
   * @param fName = the first name of account holder, given as a String.
   * @param lName = the last name of account holder, given as a String.
   * @param initBalance = the initial deposit amount (must be +ve or set to 0)
   */
  public ChequingAccount(String accountNum, String fName, String lName,
          double initBalance){
    super(accountNum, fName, lName, initBalance);
    this.chequeFee = 3.50;
    this.applyServiceFee = true;
    this.billFee = 5.50;
    this.withdrawFee = 3.50;
  }//Constructor, all arguments

  public ChequingAccount(){
    this("0","Unknown","Unknown",0);
  }//default constructor, no arguments given
  
  @Override
  /**************************************
   * billPayment
   *    Subtracts from balance the amount and the billFee and places
   *    the amount into the account specified.
   *
   * @param amount = amount of the bill (positive double)
   * @param g = account to pay
   * @return true if the bill payment was successful
   */
  public boolean billPayment(double amount, GeneralAccount g){
    if (amount > 0){
      double totalW;

      if (applyServiceFee)
        totalW = amount + billFee;
      else
        totalW = amount;

      if (balance > totalW){
        balance = balance - totalW;
        g.deposit(amount);

        //might change minBalance
        if (balance < minMonthBalance)
          minMonthBalance = balance;
        return true;
      }
      else{
        System.out.printf("bill payment error: amount and service fee"
            + " (%.2f) is greater than balance\n", totalW);
        return false;
      }
    }// outer if
    else{
      System.out.printf("bill payment error: amount (%.2f) should be "
          + "positive\n", amount);
      return false;
    }
  }// billPayment
  
  @Override
  /**************************************
   * chequeCashed
   *    Takes the amount out of the account and applies the
   *    cheque fee.
   *
   * @param num = the cheque number (an int)
   * @param amount = amount of the cheque (positive double)
   * @return true if the cheque was successfully cashed
   */
  public boolean chequeCashed(int num, double amount){
    if (amount > 0){
      double totalW;

      if (applyServiceFee)
        totalW = amount + chequeFee;
      else
        totalW = amount;

      if (balance > totalW){
        balance = balance - totalW;
        
        chequesCashed += " " + num;

        //might change minBalance
        if (balance < minMonthBalance)
          minMonthBalance = balance;
        return true;
      }
      else{
        System.out.printf("cheque cashed error:  amount and service fee"
            + " (%.2f) is greater than balance\n", totalW);
        return false;
      }
    }// outer if
    else{
      System.out.printf("cheque cashed error:  amount (%.2f) should be "
          + "positive\n", amount);
      return false;
    }
  }// chequeCashed
  
  @Override
  /**************************************
   * endOfMonth
   *    Prints report and wipes record of cheques cashed. 
   */
  public void endOfMonth(){
  super.endOfMonth();
  chequesCashed = "";
  }// endOfMonth
  
  @Override
  /**************************************
   * toString
   *    Gives String representation of an account
   *
   * @return the String representation
   */
  public String toString(){
    String res = super.toString() + "Cheques cashed:";
    if("".equals(chequesCashed)){
      res += " none\n";
    } else res += chequesCashed + "\n";
    
    return res;
  }// toString
 
}
