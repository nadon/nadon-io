/**
 * HotPotato.java
 * by Philippe Nadon
 * March 15, 2018 (last modified March 16, 2018)
 *
 * Uses a modified version of the provided CicularlyLinkedList class to
 * simulate a Hot Potato game.
 */

package hotpotato;

import java.util.Random;
import java.util.Scanner;

/**
 *  An implementation of the game Hot Potato.
 *  Separated between the setup process which contains all input scenarios
 *  and the game process, which runs until a winner is declared
 *
 *  @author Philippe Nadon
 */
public class HotPotato {

  public static void main(String[] args) {

    CircularlyLinkedList<String> playerList= new CircularlyLinkedList<>();

    setup(playerList);
    runGame(playerList);

  } // main

  /**
   * A preferable shortcut to printing messages.
   * @param message The message being printed.
   */
  private static void print(String message){

    System.out.print(message);
  } // print

  /**
   * Contains the setup process, omitted during testing.
   * @param playerList The playerList being modified.
   */
  private static void setup(CircularlyLinkedList<String> playerList){

    Scanner sc = new Scanner(System.in);
    print("Please enter the number of players: ");
    int numPlayers = sc.nextInt();

    for ( int i = 1; i <= numPlayers; i ++){

      print("Enter name of player "+i+": ");
      String playerName = sc.next();
      playerList.addLast( playerName );
    }// for

    print("Players:  "+playerList.toString());

    print("\n\nSwitch who: ");
    String player1 = sc.next();
    print("With: ");
    String player2 = sc.next();
    playerList.exchange(player1,player2);
    
    print("\nPlayers:  "+playerList.toString() +
        "\n=====================");

  }// setup

  /**
   * Plays Hot Potato until a winner is declared.
   * Does not require inputs.
   * @param playerList The list of players used to play.
   */
  private static void runGame(CircularlyLinkedList<String> playerList){

    Random rndm = new Random();

    while( playerList.size() > 1){

      int potatoMoves = rndm.nextInt(20) + 1;

      print("\nRandom time: " + potatoMoves);

      for( int i = 1; i < potatoMoves; i++){

        print("\nHas potato: " + playerList.first());
        playerList.rotate();
      }//for

      print("\nHas potato: " + playerList.first());
      playerList.removeFirst();
      print("\nSTOP!\nPlayers left:  "+playerList.toString() +
          "\n=====================");
    }// while

    print("\nGame over!\nThe winner is: " + playerList.first());
  }// runGame
}//HotPotato
