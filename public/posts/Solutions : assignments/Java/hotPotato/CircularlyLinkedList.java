/*
 * Copyright 2014, Michael T. Goodrich, Roberto Tamassia, Michael H. Goldwasser
 *
 * Developed for use with the book:
 *
 *    Data Structures and Algorithms in Java, Sixth Edition
 *    Michael T. Goodrich, Roberto Tamassia, and Michael H. Goldwasser
 *    John Wiley & Sons, 2014
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Modified by Philippe Nadon, on March 16,2018
 */

package hotpotato;

/**
 * An implementation of a circularly linked list.
 *
 * Modified for a Hot Potato game
 *
 * @author Michael T. Goodrich
 * @author Roberto Tamassia
 * @author Michael H. Goldwasser
 * @author Philippe Nadon
 */
public class CircularlyLinkedList<E> {
  //---------------- nested Node class ----------------
  /**
   * Singly linked node, which stores a reference to its element and
   * to the subsequent node in the list.
   */
  private static class Node<E> {

    /** The element stored at this node */
    private E element;

    /** A reference to the subsequent node in the list */
    private Node<E> next;

    //Constructor
    /**
     * Creates a node with the given element and next node.
     *
     * @param e  the element to be stored
     * @param n  reference to a node that should follow the new node
     */
    public Node(E e, Node<E> n) {
      element = e;
      next = n;
    }

    // Accessor methods
    /**
     * Returns the element stored at the node.
     * @return the element stored at the node
     */
    public E getElement() {
      return element;
    }

    /**
     * Returns the node that follows this one (or null if no such node).
     * @return the following node
     */
    public Node<E> getNext() {
      return next;
    }

    // Modifier methods
    /**
     * Sets the node's next reference to point to Node n.
     * @param n    the node that should follow this one
     */
    public void setNext(Node<E> n) {
      next = n;
    }
  } //----------- end of nested Node class -----------

  // instance variables of the CircularlyLinkedList
  /** The designated cursor of the list */
  private Node<E> tail = null;           // we store tail (but not head)

  /** Number of nodes in the list */
  private int size = 0;

  //Constructor
  /** Constructs an initially empty list. */
  public CircularlyLinkedList() {}

  // access methods
  /**
   * Returns the number of elements in the linked list.
   * @return number of elements in the linked list
   */
  public int size() {
    return size;
  }

  /**
   * Tests whether the linked list is empty.
   * @return true if the linked list is empty, false otherwise
   */
  public boolean isEmpty() {
    return size == 0;
  }

  /**
   * Returns (but does not remove) the first element of the list
   * @return element at the front of the list (or null if empty)
   */
  public E first() {
    if (isEmpty())
      return null;
    return tail.getNext().getElement();  // the head is *after* the tail
  }

  /**
   * Returns (but does not remove) the last element of the list
   * @return element at the back of the list (or null if empty)
   */
  public E last() {
    if (isEmpty())
      return null;
    return tail.getElement();
  }

  // update methods
  /**
   * Rotate the first element to the back of the list.
   */
  public void rotate() {
    if (tail != null)                // if empty, do nothing
      tail = tail.getNext();         // the old head becomes the new tail
  }

  /**
   * Adds an element to the front of the list.
   * @param e  the new element to add
   */
  public void addFirst(E e) {
    if (size == 0) {
      tail = new Node<>(e, null);
      tail.setNext(tail);             // link to itself circularly
    }
    else {
      Node<E> newest = new Node<>(e, tail.getNext());
      tail.setNext(newest);
    }
    size++;
  }

  /**
   * Adds an element to the end of the list.
   * @param e  the new element to add
   */
  public void addLast(E e) {
    addFirst(e);             // insert new element at front of list
    tail = tail.getNext();   // now new element becomes the tail
  }

  /**
   * Removes and returns the first element of the list.
   * @return the removed element (or null if empty)
   */
  public E removeFirst() {
    if (isEmpty())          // nothing to remove
      return null;
    Node<E> head = tail.getNext();
    if (head == tail)
      tail = null;   // must be the only node left
    else
      tail.setNext(head.getNext());  // removes "head" from the list
    size--;
    return head.getElement();
  }

  /**
   * @author Philippe Nadon
   * switches the positions of two players
   * @param object1 the first switched object
   * @param object2 the second switched object
   */
  public void exchange(E object1, E object2){

    if ( doSimpleCases(object1, object2) ) return;

    Node<E> prevNode = tail;
    Node<E> currNode = prevNode.getNext();
    Node<E> nextNode = currNode.getNext();

    for( int i = 0; i < size; i++){

      if( currNode.element.equals(object1) ||
          currNode.element.equals(object2)){

        if(nextNode.element.equals(object1) ||
            nextNode.element.equals(object2)){

          adjacentSwap(prevNode);

          return;
        }// if

        // nextNode has already been checked, so it becomes the previous
        Node<E> otherPrevNode = nextNode;

        // Iterate through the remaining unchecked Nodes
        for ( int j = 0; j < size - (i+2) ; j++){

          Node<E> otherCurrNode = otherPrevNode.getNext();

          if( otherCurrNode.element.equals(object1) ||
              otherCurrNode.element.equals(object2)){

            distantSwap(prevNode, otherPrevNode);

            return;
          }// if

          otherPrevNode = otherCurrNode;
        }// inner for
      }// outer if

      prevNode = currNode;
      currNode = nextNode;
      nextNode = nextNode.getNext();
    }// outer for
  }// exchange

  /**
   * @author Philippe Nadon
   * Swaps the objects if the scenario is simple enough
   * @param object1 first object to be swapped
   * @param object2 second object to be swapped
   * @return true if the swap was successful
   */
  private boolean doSimpleCases(E object1, E object2){

    if( size < 2) return true;

    if( object1.equals(object2)) return true;

    if( size == 2){
      rotate();
      return true;
    }

    Node<E> head = tail.getNext();
    boolean tailIsObject =
        tail.element.equals(object1) || tail.element.equals(object2);
    boolean headIsObject =
        head.element.equals(object1) || head.element.equals(object2);

    if( tailIsObject && headIsObject){
      Node<E> preTail = head;
      //To get the Node behind the tail, we need to loop all the way back
      while( preTail.getNext() != tail) preTail = preTail.getNext();

      adjacentSwap(preTail);
      return true;
    }// if

    return false;
  }// doSimpleCases

  /**
   * @author Philippe Nadon
   * Performs a swap on adjacent nodes
   * @param prevNode the node prior to the two nodes being swapped
   */
  private void adjacentSwap(Node<E> prevNode){

    Node<E> swapNode1 = prevNode.getNext();
    Node<E> swapNode2 = swapNode1.getNext();
    Node<E> nextNode = swapNode2.getNext();

    swapNode1.setNext(nextNode);
    swapNode2.setNext(swapNode1);
    prevNode.setNext(swapNode2);

    if( swapNode1 == tail) tail = swapNode2;
    else if( swapNode2 == tail) tail = swapNode1;
  }// adjacentSwap

  /**
   * @author Philippe Nadon
   * Performs a swap on Nodes which have other Nodes between them
   * @param prevNode1 The Node prior to one of the Nodes being swapped
   * @param prevNode2 The Node prior to one of the Nodes being swapped
   */
  private void distantSwap(Node<E> prevNode1, Node<E> prevNode2){

    Node<E> swapNode1 = prevNode1.getNext();
    Node<E> swapNode2 = prevNode2.getNext();
    Node<E> nextNode1 = swapNode1.getNext();
    Node<E> nextNode2 = swapNode2.getNext();

    prevNode1.setNext(swapNode2);
    swapNode2.setNext(nextNode1);

    prevNode2.setNext(swapNode1);
    swapNode1.setNext(nextNode2);

    if( swapNode1 == tail) tail = swapNode2;
    else if( swapNode2 == tail) tail = swapNode1;
  }// distantSwap

  /**
   * Produces a string representation of the contents of the list.
   * This exists for debugging purposes only.
   */
  public String toString() {
    if (tail == null)
      return "()";

    StringBuilder sb = new StringBuilder("(");
    Node<E> walk = tail;
    do {
      walk = walk.getNext();
      sb.append(walk.getElement());
      if (walk != tail)
        sb.append(", ");
    } while (walk != tail);
    sb.append(")");
    return sb.toString();
  }
}