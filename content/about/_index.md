+++
date = "2018-09-09"
draft = false
title = "About Me"

+++

### Hello!

Thank you for taking the time to visit my site. Building websites is a rather new experience so it isn't much to look at for now, but gradually I will add more to it.

### Introduction
I am a student at the [University of Alberta](https://www.ualberta.ca), with a double major in Math/Physics (that's one major) and Computing Science.
I grew up on a farm near Millet, and throughout my childhood had a passion for optimization; whether it was chores, teamwork, or even the organization of items on my desk.

I enjoy 3D modeling, a touch of pixel art, and various outdoor activities such as hiking and swimming.

I speak both English and French fluently, and have also taken a course in Latin.

### Languages
Currently, the programming languages I am familiar with are **C**, **Python**, **Java**, and **Javascript**/**HTML**, while I am aiming to learn **Swift** and **Haskell** in the coming months. I am also very interested in databases, and so **PostgreSQL** is also on my list of things to learn.

### And More
[My brother has a website](https://nadonvinyl.ca) which he uses to reach out to people who may by interested in **custom clothing and decals**. Make sure to check it out at [nadonvinyl.ca](https://nadonvinyl.ca) if you're interested and give him your support!

