---
title: "Siri Shortcuts for Augustana"
date: 2018-10-02T11:19:29-06:00
draft: false
toc: true
tags: ["Siri Shortcuts","iOS"]
series: ["other"]
categories: ["dev"]
img: "posts/sirishortcuts/sirishortcuts.jpg"
toc: true
---

{{< youtube Sy2UhidFJpw>}}

Welcome to my collection of Siri Shortcuts for the Augustana Campus of the University of Alberta!

## Contents

* [Prerequisites](#prerequisites)
* [Installation](#installation)
* [Shortcuts](#shortcuts)

## Prerequisites


To install and use Siri Shortcuts you will need,

1. the [Shortcuts app](https://itunes.apple.com/ca/app/shortcuts/id915249334?mt=8) from the iOS App Store

2. be running [iOS 12.0](https://www.apple.com/ios/ios-12/) or later. Once you've done that, you're ready to start installing some shortcuts!

## Installation


To install a siri shortcut,

1\. tap on the link

2\. tap on "get shortcut"

3\. tap "open" when it asks you to "Open in "Shortcuts""

4\. You should now see it in your library. Tap on the new shortcut's 3 dots

<img src="/posts/sirishortcuts/step2.png"></a>

5\. Press play to make sure it works, then tap on the top-right settings button

<img src="/posts/sirishortcuts/step3.png"></a>

6\. tap "Add to Siri" and then speak a phrase you want to use to prompt this shortcut

<img src="/posts/sirishortcuts/step4.png"></a>

7\. You're done!

## Shortcuts


<a href="https://www.icloud.com/shortcuts/65650f5dd9fa499eb3db57c647e818ce" target="_blank"><img src="/posts/sirishortcuts/Breakfast.png"></a>
<a href="https://www.icloud.com/shortcuts/30fbe822acf54f4e839d38c5c5cd15d7" target="_blank"><img src="/posts/sirishortcuts/Lunch.png"></a>
<a href="https://www.icloud.com/shortcuts/f39a232ee5b343a7a73d7d88f1b6b24f" target="_blank"><img src="/posts/sirishortcuts/Supper.png"></a>



