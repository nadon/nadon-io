---
title: "Bighorn Dam"
date: 2019-01-18T15:03:04-07:00
draft: false
tags: ["Abraham Lake", "Bighorn Dam", "Javascript"]
series: ["other"]
categories: ["dev"]
img: "posts/bighorndam/windy-point.JPG"
toc: true
---

**I created an interactive map, showing the changes that occured to the surrounding area after Bighorn Dam was created. Bighorn Dam is a dam in Clearwater County, Alberta, Canada, which led to the creation of Abraham Lake.**

[Here is a link to the project](https://bighorn-dam.herokuapp.com/index.html). 

