---
title: "Hackathon 2018 Submission"
date: 2018-09-09T20:47:27-06:00
draft: false
toc: true
tags: ["Hackathon","Javascript"]
series: ["other"]
categories: ["dev","competition"]
img: "posts/hackathon/Hackathon.jpg"
toc: true
---
This year I participated at the [2018 Calgary Hackathon](http://www.calgary.ca/CS/IIS/Pages/hackathon.aspx), where the theme was on improving pedestrian comfort in the city. [This was my submission](https://sites.google.com/view/etatool), which was hosted on [CodePen](www.codepen.io).
