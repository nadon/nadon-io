+++
date = "2018-11-18T11:31:21-07:00"
draft = false
title = "Projects"

+++

The `Projects` section is where i post content relevant to projects I've worked on or am currently working on. This may include competition submissions, small programs which solves a problem I came across, or long-term projects which may or may not ever come to completion.