+++
date = "2018-11-18T11:31:21-07:00"
draft = false
title = "Blog"

+++

The `Blog` section is where I post events or content which is not immediately related to a project I'm working on, or a solution to a math / comp-sci problem: