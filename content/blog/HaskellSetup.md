---
title: "Installing and Using Haskell on MacOS"
date: 2018-10-25T18:06:50-06:00
draft: false
toc: true
tags: ["Haskell"]
series: ["tutorials"]
categories: ["tutorial","dev"]
img: "posts/haskell/haskell.png"
toc: true
---

**Hello again,**

With this guide I will be showing you how to install [Haskell][1] on your Mac, a process which I personally found challenging due to the many conflicts between newer version of MacOS and the current versions of GHC / Cabal / Stack.

## Contents

* [HomeBrew](#homebrew)
* [Stack](#stack)
* [Intero](#intero)
* [VSCode](#vscode)
* [Setup](#setup)
* [Finish](#finish)

## HomeBrew
 
To start off, we’re going to need the following:

If you don’t already have [HomeBrew][8], I strongly suggest getting it even if you aren’t installing Haskell.
Install it by entering the following commands into your terminal:
{{< highlight bash >}}
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)”
{{< /highlight >}}

HomeBrew is an amazing package manager, which makes installing Stack and other tools effortless.

## Stack
 
Using [Stack][9] is preferred over Cabal due to the many problems associated with the latter. [Stack][10] can be used to install and use [GHC][11], which is the Haskell compiler we’ll be using.

Stack can be installed by entering the following commands into your preferred terminal:
{{< highlight bash >}}
brew update
brew install haskell-stack
{{< /highlight >}}

Verify that the installation succeeded by running
{{< highlight bash >}}
stack —version
{{< /highlight >}}

## Intero
 
[Intero][12] is required to run [Haskero][13], our extension for [Visual Studio Code][14] which gives us a comfy environment for Haskell development.

Now that we have Stack installed, we’re going to use it to install Intero with the following terminal command:
{{< highlight bash >}}
stack build intero
{{< /highlight >}}

To test whether it installed correctly, run
{{< highlight bash >}}
stack exec intero
{{< /highlight >}}

## VSCode
 
Our final download is [Visual Studio Code][15], and some extensions:

To do so, make sure HomeBrew is installed and run the following terminal command:
{{< highlight bash >}}
brew cask install visual-studio-code
{{< /highlight >}}

If the installation is successful you will see Visual Studio Code appear in your apps. Launch it and then install the following extensions from the extensions tab:

- [Haskell Syntax Highlighting][16]
- [hoogle-vscode][17]
- [Haskero][18]

## Setup
 
Now we’re going to get your development environment set up:

To start a new Haskell project, run the following terminal commands:

{{< highlight bash >}}
stack new name-of-project
cd name-of-project
stack setup
{{< /highlight >}}

Now we can open this folder in Visual Studio Code. When you open a Haskell source file, you should see syntax highlighting and other useful features appear. If not you may have made an error in following the prior steps, or this guide is outdated / has a mistake. If the latter seems to be the case, don’t be afraid to contact me.

Once you have made a project to test, or you just want to run the prebuilt test file, run the following in terminal:

{{< highlight bash >}}
stack build
stack exec my-project-exe
{{< /highlight >}}

## Finish
 
There are a few finishing notes to consider:

If you want to run Haskell interactively in the terminal, enter:
{{< highlight bash >}}
ghci
{{< /highlight >}}

Try to avoid using Cabal, Stack is essentially the same but with many improvements, overall you will have a less buggy experience.

If you're a die-hard terminal-based developer, I would strongly advise using emacs, with spacemacs optionally installed on top. Intero is supposed to work great with either option straight out of the box, although I have had less luck. I prefer VSCode due to its easier learning curve and great feature set.

That is mostly what I have to say about this, if you have any questions or concerns feel free ot contact me, otherwise happy coding!

[1]:	https://Haskell.org
[8]:	https://brew.sh
[9]:	http://seanhess.github.io/2015/08/04/practical-haskell-getting-started.html
[10]:	http://seanhess.github.io/2015/08/04/practical-haskell-getting-started.html
[11]:	https://wiki.haskell.org/GHC
[12]:	https://github.com/commercialhaskell/intero/blob/master/TOOLING.md#installing
[13]:	https://marketplace.visualstudio.com/items?itemName=Vans.haskero
[14]:	https://code.visualstudio.com
[15]:	https://code.visualstudio.com
[16]:	https://marketplace.visualstudio.com/items?itemName=justusadam.language-haskell
[17]:	https://marketplace.visualstudio.com/items?itemName=jcanero.hoogle-vscode
[18]:	https://marketplace.visualstudio.com/items?itemName=Vans.haskero