---
title: "Set Up a Website for Free!"
date: 2018-10-26T18:05:14-06:00
lastmod: "2018-11-18-06:00"
draft: false
toc: true
tags: ["Website"]
series: ["tutorials"]
categories: ["tutorial","dev"]
img: "posts/website/website.png"
toc: true
---

**This is a tutorial on building a free website using a mac**

I will be going step-by step on the tools / services you'll need to get a website set up with next to no programming experience. For those of use using a Windows-based machine, it should be virtually the same process.

## Contents
 
* [Motivation](#motivation)
* [Hugo](#hugo)
* [Using Hugo](#setup)
* [GitHub](#github)
* [Netlify](#netlify)
* [Domain (optional)](#domain)
* [Finish](#finish)

## Motivation
 
This is obviously not the only guide on building a website. It is however, one of the few that offer a 100% free option that still allows you to access the source code *AND* does not put ads on your site. The only portion (which is optional) that requires payment is buying a domain name, otherwise your site address will be just as long and ugly as any other free option.

## Hugo
 
We will use [HomeBrew][8] to install [Hugo][9], which is a framework for building websites.

In Terminal ( or command prompt for Windows), enter the following command:

```shell
brew install huge
```

Verify that this is correct by entering

```shell
hugo version
```


Now that you have Hugo installed, you can create a locally hosted site by entering

```shell
hugo new site name-of-project
```


Now you're going to want to [install a theme][10], a friend of mine chose `hugo-now-ui` as an example to use in this tutorial, if you wish to use a different one, simply replace `hugo-now-ui` and its GitHub URL with the one corresponding to the theme you chose, which you can find on its page under `Installation`:

```shell
cd name-of-project
git init;
git submodule add https://github.com/cboettig/hugo-now-ui themes/hugo-now-ui
echo 'theme = "hugo-now-ui"' >> config.toml
```


[Submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules) are a little tricky and any errors that come up might be difficult to fix( I don't even try, I just nuke the [repository](https://help.github.com/articles/about-repositories/)). Just make sure not to modify the submodule at all, and just leave it the way it is. You shouldn't have to change anything in there, all you'll be editing is the config files, and things in the content/static folders.

## Setup
 
First thing you're going to want to do is edit the config.toml file in the root directory of your new project. [This is a good guide][11] for configuring your website, or you can look at [my own config.toml][12] for this website.

Now we can add a new post, by entering

```shell
hugo new posts/new-page.md
```


Hugo will then automatically place a new page in the category "posts". In the case of this tutorial page, I entered

```shell
hugo new posts/sitesetup.md
```


Now you can edit the new Markdown file with your favorite editor( I recommend [sublime text 3][13], it's great). Regardless of your proficiency with Markdown I suggest looking at [this page][14] for what Hugo calls "Shortcodes", code snippets which allow you to do cool things like embed [twitter posts][15] and [youtube videos][16], or the way I formatted the terminal commands in this tutorial.

Now that you've made a new post, let's test the result:

```shell
hugo server -D
```


The -D displays pages even with draft=true, if you want to see the way your site would actually look, just omit the -D.

By default your local site will be available at http://localhost:1313/, but check the output in the terminal to make sure.

## GitHub
 
If you don't already have a [GitHub][17] account, make one. Once you have done so, make a new repository. This is where the data for your website will be uploaded to from your computer, and then pushed to your website deployer.

I recommend the [Github Desktop][18] app, which is basically the same as terminal-based git commands, but with a gui, making it alot easier for those of us who prefer that.

Now that you have the repository set up, you can build the site and then push it to GitHub. Generally the procedure to make new posts on your website is a 4-step process; *edit, build with Hugo, push to GitHub, deploy on Netlify*.

We're currently at the *build with Hugo* step, to do so make sure you're in the root directory of your project and enter

```shell
hugo
```

into your terminal.

Hopefully your site builds with no issue, after which you can commit and push to GitHub using the GitHub Desktop app.

## Netlify
 
Now we have to connect your repository to [Netlify][19], to deploy your site. Create an account using your GitHub credentials, and follow the steps until you're ready to connect your repository. 

You will need to make a netlify.toml file in the same directory as your config.toml file, this will contain information for Netlify to use when deploying your site. Follow the instructions on [this page][20] to get it set up, and/or look at [my own netlify.toml][21]. You can ignore the "Use Hugo Themes with Netlify" section. After following these steps you should have a working website at the address given to you by Netlify.

## Domain
 
If you're absolutely adamant about not paying a dime for your site, then you can skip this section.
If you're like me you love free stuff but are still picky about having a nice domain address, in which case paying something like $12 a year seems reasonable for a nice, short URL. Services like [Go Daddy][22] or [Google Domains][23] all basically get the job done, I use the latter for the custom email (that costs extra), but [Go Daddy][24] is well known to be a very good domain provider. I believe Netlify also offers domain names, but I could be wrong.

After finding and purchasing your domain name, head over to your site's settings page *on Netlify*. The steps should be similar for all, but I will be going through configuring Google Domains. If you ever get confused, [this guide][25] can supplement my own.

Go down to *custom domains*, and add your own domain address. Afterwards, make sure to get an SSL certificate at the bottom, for that secure encryption.

Once you've done that, head over to your Google Domains settings page, and switch to the DNS section. 

Copy the site address given to you by Netlify, and add it as a *custom resource record*:
NAME=www, TYPE=CNAME, TTL=1h, and DATA=(your netlify site address).

Optionally, add the following resource record as well:
NAME=@, TYPE=A,TTL=1h, DATA=104.198.14.52

## Finish
 
This guide is definitely in its early stages, and so if there is a step in which you had trouble getting something to work, there are many ways to get help:

First and foremost, check out the guides on Netlify and Hugo, they have amazing guides that are almost always up to date.

If you're still having trouble or you're fairly certain your problem lies with Netlify, contact the support team at Netlify, *they are incredibly helpful and response surprisingly quickly*.

If you're still having problems and/or suspect that my guide might be at fault (sorry), I would appreciate it if you contacted me, I usually respond anywhere between minutes or a few hours depending on the time of day and assuming I'm not busy with something.

[8]:	https://brew.sh
[9]:	https://gohugo.io/getting-started/quick-start/
[10]:	https://themes.gohugo.io
[11]:	https://gohugo.io/getting-started/configuration/
[12]:	https://www.dropbox.com/s/umzisyghb63lmb6/config.toml?dl=0
[13]:	https://www.sublimetext.com
[14]:	https://gohugo.io/content-management/shortcodes/
[15]:	https://nadon.io/posts/12180918/
[16]:	https://nadon.io/projects/augshortcuts/
[17]:	https://github.com
[18]:	https://desktop.github.com
[19]:	https://www.netlify.com
[20]:	https://gohugo.io/hosting-and-deployment/hosting-on-netlify/
[21]:	https://www.dropbox.com/s/zcxzl09up0mbgqp/netlify.toml?dl=0
[22]:	https://ca.godaddy.com
[23]:	https://domains.google/#/
[24]:	https://ca.godaddy.com
[25]:	https://www.netlify.com/docs/custom-domains/