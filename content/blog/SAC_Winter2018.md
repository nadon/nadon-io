---
title: "SAC Winter 2018 presentation"
date: 2018-09-18T12:20:17-06:00
draft: false
toc: true
tags: ["Presentation","monte carlo", "markov chain"]
series: ["other"]
categories: ["mathematics"]
img: "posts/monteCarlo/monteCarlo.png"
toc: true
---

First off, I love presentations. In the past I have presented at the Student Academic Conference (SAC), and will continue to do so. My topics of interest usually revolve around applicable mathematical or physical phenomena which can be solved or explained using software. My latest presentation is an [introduction to Markov Chain Monte Carlo methods](https://docs.google.com/presentation/d/1kJmnJEuul5wm3hczZffMg9Vshu_T5Wjpe9WPUY05X_g/edit#slide=id.p3), with an accompanying [link to the software repository at Github](https://github.com/pnadon/PresentationExamples).