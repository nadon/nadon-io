---
title: "My Favorite Tools for Development"
date: 2018-10-22T18:06:14-06:00
draft: false
toc: true
tags: ["C","Python","Java","Scheme","Haskell","Website"]
series: ["tutorials"]
categories: ["tutorial","dev"]
img: "posts/favoritedevtools/favdevtools.png"
toc: true
---

**Hello! I'm making this post to explain the tools I'm currently using on a Mac running 10.14 (Mojave).**

This isn't quite a tutorial but I'll make sure to post all relevant links to downloads, and more in-depth tutorials for certain setups which require more work, such as Haskell.

(credit to u/madakikoeru on Reddit for the thumbnail image of this post)

## Contents

* [HomeBrew](#homebrew)
* [Terminal](#terminal)
* [Python](#python)
* [Java](#java)
* [C](#c)
* [Scheme](#scheme)
* [Haskell](#haskell)
* [Website](#website)

## HomeBrew
 
Before anything else, I advise installing [HomeBrew][9], a package manager for MacOS. The convenience of typing a few words into a terminal and having a program or tools installed automatically is something you simply cannot go without. Follow the instructions on the site or just run 
{{< highlight bash >}} 
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" 
{{< /highlight >}}
 to install it on your system. For Windows, I advise [Chocolatey][10]

## Terminal
 
My favorite terminal so far is [Upterm][11], due to the great features that come built in, such as search and autocomplete. The loading time is somewhat slow, but I usually leave it open anyways. To install, if you haven't installed HomeBrew as described above, I would highly suggest to do so. Then, in terminal type:
{{< highlight bash >}} 
brew install upterm
{{< /highlight >}}
This will automatically install Upterm on your system.

## Python
 
The setup I found most ideal for Python is using [Sublime Text][12] along with the [Python 3][13], [SublimeLinter][14], and [SublimeLinter-flake8][15]. For the flake8 package to work properly you will need to install flake8 via the command "pip install flake8" in the terminal.

The result will look something like this:

<img src="/posts/favoritedevtools/SublimeTextPython.gif"></a>

Sublime Text 3 is overall my favorite text editor for every day scenarios, and for any project that involves only a few files. The boot speed and smoothness is beyond anything else I have used, and so unless I want to use a full-blown IDE, Sublime Text is the first thing I try to setup for development.

## Java
 
[Intellij Idea by JetBrains][16] is hands down my favorite IDE for Java, as well as others such as [Scala][17], [Kotlin][18], and any others that might involve large project sizes and require all the features that a top-notch IDE brings.

Most languages that are not supported with the base installation can be found in the Plugins Repository, and are generally free of manual installation or configuration.

Here are the plugins I use:

- [Scala][19]
- [NodeJS][20]
- [Material UI][21]
- [Kotlin][22]

C
---

With C there isn't much to say, I personally find [Xcode][23] to work best for me, although [CLion][24] would be a close second. The debugging tools, auto-complete and OS-integrated dark mode make for a comfortable IDE which fills my requirements and does a good job of staying out of the way of my work. The installation may take a while as it also installs other tools that are useful for development. *Even if you don't use Xcode for development, I would still suggest installing it for the other tools which come with it*.

## Scheme
 
I have had an interest in functional languages recently, and Petite Chez Scheme is a good place to start. If you haven't already, install [brew][25] (or [chocolatey][26]) as described above. Then, in your terminal, type:
{{< highlight bash >}} 
brew install chezscheme
{{< /highlight >}}
This should install Scheme on your computer, which you can test by then typing "petite" in your terminal. If successful, you will be running in the interactive environment of Petite Chez Scheme. Type "(exit)" to exit.

Next, we're going to want to setup a comfortable dev environment. While I couldn't find any good packages for Sublime Text, a very good alternative is [Visual Studio Code][27], which feels less like a text editor and more like a lite IDE. It even has a pre-installed debugger and integrated terminal.

The packages I installed to develop Scheme inside of Visual Studio Code are the following: [Material Icon Theme][28], [Material Theme kit][29], [One Dark Pro][30], and [vscode scheme][31].

## Haskell
 
Haskell is a great language to get into, especially to reinforce good coding practices and a more recursive way of thinking. Haskell was trickier to install than anything above. [Here is a link to the full tutorial][32].

## Website
 
Website development is very subjective and I am in no way an expert. I managed to create a website using free tools, and a paid website domain through [Google Domains][33]. The other services I used *which were all free*, were [Hugo][34] for content creation, [Github][35] for pushing updates to the site, and finally [Netlify][36] to build and deploy the website. With this setup you are able to create and maintain your site with almost zero programming knowledge, and without paying a dime (minus the domain name, unless you want to use the long one Netlify gives you).

[Here is the full tutorial on how to accomplish this][37]

[9]:	https://brew.sh
[10]:	https://chocolatey.org
[11]:	https://github.com/railsware/upterm
[12]:	https://www.sublimetext.com/3
[13]:	https://packagecontrol.io/packages/Python%203
[14]:	https://packagecontrol.io/packages/SublimeLinter
[15]:	https://packagecontrol.io/packages/SublimeLinter-flake8
[16]:	https://www.jetbrains.com/idea/download/#section=mac
[17]:	https://www.scala-lang.org
[18]:	https://kotlinlang.org
[19]:	https://plugins.jetbrains.com/plugin/1347-scala
[20]:	https://plugins.jetbrains.com/plugin/6098-nodejs
[21]:	https://plugins.jetbrains.com/plugin/8006-material-theme-ui
[22]:	https://plugins.jetbrains.com/plugin/6954-kotlin
[23]:	https://developer.apple.com/xcode/
[24]:	https://www.jetbrains.com/clion/
[25]:	#HomeBrew
[26]:	https://chocolatey.org
[27]:	https://code.visualstudio.com
[28]:	https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme
[29]:	https://marketplace.visualstudio.com/items?itemName=ms-vscode.Theme-MaterialKit
[30]:	https://marketplace.visualstudio.com/items?itemName=zhuangtongfa.Material-theme
[31]:	https://marketplace.visualstudio.com/items?itemName=sjhuangx.vscode-scheme
[32]:	https://nadon.io/blog/2018-10/installing-and-using-haskell-on-macos/
[33]:	https://domains.google/#/
[34]:	https://gohugo.io
[35]:	https://github.com
[36]:	https://www.netlify.com
[37]:	https://nadon.io/blog/2018-10/set-up-a-website-for-free/