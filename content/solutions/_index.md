+++
date = "2018-11-18T11:31:21-07:00"
draft = false
title = "Solutions"

+++

The `solutions` section is for posting solutions to problems related to mathematics, computing science, and occasionally physics.